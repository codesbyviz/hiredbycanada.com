<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use App\Setting;
class SettingsProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Settings
        Blade::directive('settings', function($settingsItem) {
            $settings = Setting::where('option',$settingsItem)->first();
            return ($settings) ? $settings->value :"Invalid Settings";
        });
        $settings =  Setting::all();
        return $settings;
    }
}
