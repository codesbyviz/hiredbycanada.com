<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use App\PageBlock;
class PageBlockServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('contentblock', function($title) {
            $content = PageBlock::where('title',$title)->first();
            if($content){
                return $content->content;
            }
            else{
                return "<span class=\"text-danger\">Invalid Content Block. We are unable to find this content Block</span>";
            }
        });
    }
}
