<?php

namespace App\Http\Controllers\Admin;

use FFMpeg;
use Storage;
use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $videos = Video::orderBy('position','asc')->paginate(($request->count)?$request->count:10);
        return view('admin.videos.index',['videos'=>$videos]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.videos.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "video"=>"required|file|",
            "title"=>"required|max:255|",
            "description"=>"required|string",
            "activity"=>"required|string",
            "position"=>"required|min:1",
            "thumbnail"=>"nullable|file",
            "create_at"=>"nullable|int|min:1",
        ]);
        $video = new Video;
        $filename = uniqid();
        $savedOriginal      = $request->file('video')->store('videos','public');
        $video->video       = $savedOriginal;
        // $video->video       = FFMpeg::fromDisk('public')->open($savedOriginal)->export()->inFormat(new FFMpeg\Format\Video\WebM)->save($filename.'.webm');
        // Storage::disk('public')->delete($savedOriginal);
        if($request->hasFile('thumbnail')){
            $video->thumbnail       = $request->file('thumbnail')->store('videos','public/thumbnails');
        }
        else{
            FFMpeg::fromDisk('public')
            ->open($video->video)
            ->getFrameFromSeconds($request->create_at)
            ->export()
            ->toDisk('public')
            ->save("thumbnails/$filename.png");
            $video->thumbnail = "thumbnails/$filename.png";
        }

        $video->title       = $request->title;
        $video->description = $request->description;
        $video->activity    = $request->activity;
        $video->position    = $request->position;
        $video->save();
        return redirect()->route('modules.index')->with('success',"New Module Attached");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show($video)
    {
        $video = Video::findorfail($video);
        return view('admin.videos.edit',['video'=>$video]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        return view('admin.videos.edit',['video'=>$video]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $video)
    {
        $video = Video::findorfail($video);
        $request->validate([
            "title"=>"required|max:255|",
            "description"=>"required|string",
            "activity"=>"required|string",
            "position"=>"required|min:1",
            "thumbnail"=>"nullable|file",
            "create_at"=>"nullable|int|min:0",
        ]);
        if($request->hasFile('thumbnail')){
            Storage::delete($video->thumbnail);
            $video->thumbnail       = $request->file('thumbnail')->store('videos','public/thumbnails');
        }
        elseif($request->create_at >= 1){
            Storage::delete($video->thumbnail);
            FFMpeg::fromDisk('public')
            ->open($video->video)
            ->getFrameFromSeconds($request->create_at)
            ->export()
            ->toDisk('public')
            ->save("thumbnails/$video->video.png");
            $video->thumbnail = "thumbnails/$video->video.png";
        }

        $video->title       = $request->title;
        $video->description = $request->description;
        $video->activity    = $request->activity;
        $video->position    = $request->position;
        $video->save();
        return redirect()->route('modules.index')->with('success',"Module Edited");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy($video)
    {
        $video = Video::findorfail($video);
        Storage::disk('public')->delete($video->video);
        Storage::disk('public')->delete($video->thumbnail);
        $video->delete();
        return redirect()->route('modules.index')->with('success',"Module Deleted");
    }
}
