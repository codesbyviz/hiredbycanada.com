<?php

namespace App\Http\Controllers;

use App\Invite;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\InvitationEmail;
class InviteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $invites = Invite::paginate(($request->count)?$request->count:10);
        return view('admin.invites.index',['invites'=>$invites]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $emails = explode(',',$request->emails);
        $validEmails = 0;
        $invalidEmails = 0;
        foreach ($emails as $email) {
            if(filter_var( $email, FILTER_VALIDATE_EMAIL )){
                $invite = Invite::where('user_email',$email)->first();
                if(!$invite){
                    $invite = new Invite;
                    $invite->id = (string) Str::uuid();
                    $invite->user_email = $email;
                    $invite->save();
                    $validEmails++;
                }

                Mail::to($email)->send(new InvitationEmail($invite));

            }
            else{
                $invalidEmails++;
            }
        }
        return redirect()->back()->with('success',"Invited $validEmails Emails");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invite  $invite
     * @return \Illuminate\Http\Response
     */
    public function show(Invite $invite)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invite  $invite
     * @return \Illuminate\Http\Response
     */
    public function edit(Invite $invite)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invite  $invite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invite $invite)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invite  $invite
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invite $invite)
    {
        //
    }
}
