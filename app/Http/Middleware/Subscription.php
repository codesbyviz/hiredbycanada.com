<?php

namespace App\Http\Middleware;

use Closure;
use App\Subscription as Subs;
class Subscription
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        $subsrib = Subs::where('user_id',$user->id)->first();
        if($subsrib){
            return $next($request);
        }
        else{
            return redirect()->route('payment.confirm');
        }
    }
}
