<?php

namespace App;
use FFMpeg;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{

    public function FileDetails($atte)
    {
        // return FFMpeg::fromDisk('public')->open($this->video);
        return FFMpeg::fromDisk('public')->open($this->video)->getStreams()->first()->get($atte);
    }

    public function getDurationAttribute()
    {
        $media = FFMpeg::fromDisk('public')
                        ->open($this->video)
                        ->getStreams()
                        ->first()
                        ->get('duration');

        return gmdate("H:i:s", (int)$media);    
    }
}
