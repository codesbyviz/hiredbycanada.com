<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "HomeController@index");
Auth::routes(['verify' => true]);

Route::get('/page/{page}', "HomeController@page")->name('pages');
Route::any('/purchase', 'HomeController@Purchase')->name('purchase');
Route::get('/payment/confirm', 'PaymentController@index')->name('payment.confirm')->middleware('auth');
Route::post('/payment/process', 'PaymentController@process')->name('payment.process')->middleware('auth');
Route::post('/payment/invitation', 'PaymentController@invitation')->name('payment.invitation')->middleware('auth');
Route::get('/payment/confirmation', 'PaymentController@confirmation')->name('payment.confirmation')->middleware('auth');

Route::middleware(['subscribed','auth'])->group(function(){
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/access-videos', 'HomeController@access')->name('access-videos');
    Route::get('/access-video/{id}', 'HomeController@accessVideo')->name('access-video');
});

Route::prefix('admin')->group(function(){
    Route::get('', 'AdminController@index');
    Route::resource('page-blocks', 'PageBlockController');
    Route::resource('invites', 'InviteController');
    Route::resource('modules', 'Admin\VideoController');
    Route::resource('users', 'UserController');
    Route::resource('transactions', 'TransactionController');
    Route::resource('activity', 'ActivityController');
    Route::post('complete/activity/{activity}', 'ActivityController@complete')->name('activity.complete');
});