
require('./bootstrap');

window.Vue = require('vue');
import VuePlyr from 'vue-plyr'
Vue.use(VuePlyr)
const files = require.context('./components/', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));


const app = new Vue({
    el: '#app',
});
