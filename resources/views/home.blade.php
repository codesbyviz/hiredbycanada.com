@extends('layouts.app')
@section('content')
<div class="jumbotron mb-0 bg-primary" style="min-height:20vh;background-position:center;background-size:cover;background-image:url('{{asset('images/student-849824_1920.jpg')}}')">
    <div class="overlay">
        <div class="container justify-content-center align-center text-left text-light">
            <div class="py-4"></div>
            <h1 class="landing-title text-thin">Get <strong class="">Hired</strong><br/> <span class="text-thin"></span></h1>
            <p>Get started today. See results for your self.</p>
            @auth
                <a href="{{route('access-videos')}}" class="btn btn-outline-light">My Dashboard <i class="fas fa-arrow-right"></i></a>
            @else
                <a href="{{route('register')}}" class="btn btn-primary">Get Started <i class="fas fa-arrow-right"></i></a>
                <a href="{{route('login')}}" class="btn btn-light">Login <i class="fas fa-arrow-right"></i></a>
            @endauth
        </div>
    </div>
</div>
<div class="py-4 bg-white mb-4 shadow-sm">
    <div class="container text-primary">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-6 col-lg-3 text-center border-right border-primary">
                <i class="mb-3 far fa-play-circle fa-3x"></i><br/>
                <p>Access Your Content</p>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3 text-center border-right border-primary">
                <i class="mb-3 fas fa-chalkboard-teacher fa-3x"></i><br/>
                <p>Access Your Content</p>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3 text-center border-right border-primary">
                <i class="mb-3 fas fa-chalkboard-teacher fa-3x"></i><br/>
                <p>Access Your Content</p>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3 text-center">
                <i class="mb-3 far fa-check-circle fa-3x"></i><br/>
                <p>Access Your Content</p>
            </div>
        </div>
    </div>
</div>
<div class="py-3"></div>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <h3 class="text-primary mb-4"><span class="text-thin border-bottom py-2 border-primary">About</span> Ron</h3>
            <p class="text-justify about-para text-black-50">
                @contentblock(home-page-about)
                <br/>
                <a href="{{route('pages','about')}}">Read More <i class="fas fa-arrow-right"></i></a>
            </p>
        </div>
    </div>
</div>
@endsection
