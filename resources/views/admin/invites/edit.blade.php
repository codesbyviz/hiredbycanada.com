@extends('layouts.admin')
@section('content')
<div class="py-4"></div>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="float-right">
                <a href="{{route('page-blocks.index')}}" class="btn btn-dark btn-sm">Back to Pageblocks</a>
            </div>
            <h4>Edit {{$pageblock->title}}</h4>
        </div>
        <div class="col-sm-12 mt-4">
            <form action="{{route('page-blocks.update',$pageblock)}}" method="post">
                <input type="hidden" name="_method" value="PUT">
                @csrf
                <div class="form-group">
                    <label for="content">Content</label>
                    <textarea style="min-height:300px" name="content" id="content" class="editor @error('content') is-invalid @enderror">{{@old('content',$pageblock->content)}}</textarea>
                    @error('content') 
                        <span class="invalid-feedback">
                            {{$message}}
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                 <button type="submit" class="btn btn-success ">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
