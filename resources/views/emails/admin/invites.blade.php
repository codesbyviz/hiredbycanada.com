@component('mail::message')
# Congratulations! 
You are given Access to Hired By Canada's Resources.


@component('mail::button', ['url' => route('register',$invite->id)])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
