@extends('layouts.app')
@section('content')
<div class="py-4"></div>
<div class="py-4"></div>
<div class="container">
    @subscribed
    <div>
        You are already Subscribed
    </div>
    @else
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-6">
            <div class="card border-0 shadow-sm">
                <div class="card-body">
                    <h3 class="text-primary text-center text-thin my-4">Confirm Your Payment</h3>
                    <table class="table table-bordered">
                        <tr>
                            <th>Purchase</th>
                            <td class="w-50">Subscription for {{$purchase->course}}</td>
                        </tr>
                        <tr>
                            <th class="w-50">Fees</th>
                            <td class="w-50">Rs. {{$purchase->fees}}</td>
                        </tr>
                        @if (isset($purchase->coupon))
                        <tr>
                            <th class="w-50">Coupon Code</th>
                            <td class="w-50 text-success">{{$purchase->coupon->code}} <a href="{{route('payment.confirm')}}"
                                    class="text-danger"><i class="far fa-times-circle"></i> </a></td>
                        </tr>
                        <tr>
                            <th class="w-50">Discount</th>
                            <td class="w-50">- Rs. {{$purchase->coupon->discount_amount}}</td>
                        </tr>
                        @else
                        <tr>
                            <th class="w-50">Coupon Code</th>
                            <td class="w-50">
                                <form action="" method="get">
                                    <div class="input-group">
                                        <input type="text" class="form-control form-control-sm" name="code" id="code">
                                        <div class="input-group-append">
                                            <button class="btn btn-sm btn-outline-secondary" type="submit"
                                                id="code">Apply</button>
                                        </div>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        @endif
                        <tr>
                            <th class="w-50">GST <i>(@ {{$purchase->gst}}%)</i></th>
                            <td class="w-50">Rs. {{$purchase->gst_amount}}</td>
                        </tr>
                        <tr>
                            <th class="w-50">Total</th>
                            <td class="w-50">Rs. {{$purchase->total}}</td>
                        </tr>
                    </table>
                </div>
                <div class="card-footer border-0">
                <form action="{{route('payment.process')}}" method="post">
                    @csrf
                    <input type="hidden" name="fees" value="{{$purchase->fees}}">
                    <input type="hidden" name="total" value="{{$purchase->total}}">
                    <input type="hidden" name="gst" value="{{$purchase->gst}}">
                    <input type="hidden" name="gst_amount" value="{{$purchase->gst_amount}}">
                    @if (isset($purchase->coupon))
                    <input type="hidden" name="coupon" value="{{$purchase->coupon->id}}">
                    <input type="hidden" name="discount" value="{{$purchase->coupon->discount_amount}}">
                    @endif
                    <button type="submit" class="btn btn-primary shadow-sm float-right">Continue to Pay</button>
                </form>
                </div>
            </div>            
            @if (isset($purchase->codeerror))
            <div class="alert py-2 alert-danger border-0">
                <span>{{$purchase->codeerror}}</span>
            </div>
            @endif

        </div>
    </div>
    @endsubscribed
</div>


{{-- <div class="container py-4 ">
        <div class="card border-0 shadow-sm">
            <div class="card-body">
                <h1 class="text-primary text-thin my-4">Confirm Your Payment</h1>
            </div>
        </div>
    </div> --}}
@endsection
