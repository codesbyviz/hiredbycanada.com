@extends('layouts.app')
@section('content')
<div class="py-4"></div>
<div class="py-4"></div>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="text-primary mb-4"><span class="text-thin border-bottom py-2 border-primary">Privacy</span> Policy</h3>
            <p class="text-justify about-para text-black-50">
                @contentblock(privacy-policy)
            </p>
        </div>
    </div>
</div>
@endsection
