@extends('layouts.app')
@section('content')
<div class="container">
    <div class="py-1"></div>    
    <div class="row">
        <div class="col-sm-12 col-md-8">
            <video-player title="{{$video->title}}" 
                activity="{{$video->activity}}"
                video="{{Storage::url($video->video)}}" thumbnail="{{Storage::url($video->thumbnail)}}"></video-player>
            <div class="py-3"></div>
            <h2 class="text-primary mb-0">{{$video->title}}</h2>
            <small>{{Carbon::parse($video->created_at)->toFormattedDateString()}}</small>
            <p class="text-black-50">{!!$video->description!!}</p>
        </div>
    </div>
    <div class="row">

    </div>
</div>
@endsection
