<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-primary mb-0 py-0 bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand text-primary" href="{{ url('/') }}">
                    <img src="/images/hiredByCanada.png" alt="" style="width:80px;"/>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="/">{{ __('Home') }}</a>
                        </li>                        
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('pages','about')}}">{{ __('About') }}</a>
                        </li>                        
                        <li class="nav-item">
                            <a class="nav-link" href="/blog">{{ __('Blog') }}</a>
                        </li>                        
                        
                        <!-- Authentication Links -->
                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @endif
                        @else
                        @subscribed
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('access-videos')}}">{{ __('Access Videos') }}</a>
                        </li>
                        @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('payment.confirm')}}">{{ __('Complete Purchase') }} <div class="spot bg-danger"></div></a>
                        </li>
                        @endsubscribed
                        @endguest                        
                    </ul>
                    <ul class="navbar-nav ml-auto">
                        @auth
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('payment.confirm')}}"><i class="far fa-user-circle"></i> </a>
                        </li>                        
                        <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endauth
                    </ul>                    
                </div>
            </div>
        </nav>

        <main class="mh-100" style="min-height:100vh;">
            @yield('content')
        </main>
        <footer class="py-2 bg-primary text-light">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <small>All Rights Reserved. &copy; {{date('Y')}}</small>
                    </div>
                    <div class="col-sm-8 text-right">
                        <small>
                            <a class="text-light" href="{{route('pages','terms-and-conditions')}}">Terms and Conditions</a> | <a class="text-light" href="{{route('pages','privacy-policy')}}">Privacy Policy</a> |
                            <a class="text-light" href="#">Cookie Policy</a>
                        </small>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</body>
</html>
