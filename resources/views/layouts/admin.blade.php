<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.6/tinymce.min.js"></script>
    <!-- Fonts -->

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark mb-0 py-0 bg-primary">
            <div class="container">
                <a class="navbar-brand text-primary" href="{{ url('/') }}">
                    <img src="/images/white_hiredByCanada.png" alt="" style="width:80px;"/>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">

                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="/admin">{{ __('Dashboard') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('page-blocks.index')}}">{{ __('Page Blocks') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('invites.index')}}">{{ __('Free Invites') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('modules.index')}}">{{ __('Modules') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('users.index')}}">{{ __('Users') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('transactions.index')}}">{{ __('Transactions') }}</a>
                        </li>
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest                       
                    </ul>
                </div>
            </div>
        </nav>
        @if ($errors->any())
        <div class="alert alert-danger mb-0 border-0">
            {{$errors}}
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <main class="bg-white mh-100" style="min-height:100vh;">
            @yield('content')
        </main>
        <footer class="py-2 bg-dark text-light">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <small>All Rights Reserved. &copy; {{date('Y')}}</small>
                    </div>
                    <div class="col-sm-8 text-right">
                        <small>
                            <a class="text-light" href="#">Terms and Conditions</a> | <a class="text-light" href="#">Privacy Policy</a> |
                            <a class="text-light" href="#">Cookie Policy</a>
                        </small>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script>
    tinymce.init({
        selector: '.editor'
    });
    </script>
</body>
</html>
