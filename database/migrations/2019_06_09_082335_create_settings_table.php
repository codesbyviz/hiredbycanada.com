<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('option')->unique();
            $table->longText('value')->unique();
            $table->timestamps();
        });

        $settings = [
            "course_name"=>"Course Name",
            "fees"=>12500,
            "gst"=>18.5,
            "from_email"=>"no-reply@hiredbycanada.com",
        ];
        foreach ($settings as $key => $value) {
            $setting = new \App\Setting;
            $setting->option = $key;
            $setting->value  = $value;
            $setting->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
